package es.boalis.security.auth.factory;

import es.boalis.security.auth.action.CredencialValidationAction;
import es.boalis.security.auth.action.FlowAction;
import es.boalis.security.auth.exception.InstanceTypeNotFound;

/**
 * Created by santi on 7/10/17.
 */
public interface FlowFactory {

    FlowAction getInstance(String key)throws InstanceTypeNotFound;

}
