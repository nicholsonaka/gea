package es.boalis.security.auth.action;

import es.boalis.security.auth.bean.AuthenticationData;
import es.boalis.security.auth.bean.Credential;
import es.boalis.security.auth.bean.SharedData;
import es.boalis.security.auth.exception.CredentialsGenerationException;

import java.util.List;

/**
 * Created by santi on 7/10/17.
 */
public abstract class CredentialGenerationAction {

    public abstract List<Credential> getCredentias(AuthenticationData data, SharedData<String, ?> shared)
            throws CredentialsGenerationException;
}
