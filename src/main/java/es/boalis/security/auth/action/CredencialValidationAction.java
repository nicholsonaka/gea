package es.boalis.security.auth.action;

import es.boalis.security.auth.bean.AuthenticationData;
import es.boalis.security.auth.bean.SharedData;
import es.boalis.security.auth.exception.CredentialsValidationException;

/**
 * Created by santi on 7/10/17.
 */
public abstract class CredencialValidationAction {

    public abstract SharedData<String,?> getAuthenticationData(AuthenticationData data,SharedData<String,?>shared)
            throws CredentialsValidationException;
}
