package es.boalis.security.auth.action;

import es.boalis.security.auth.bean.SharedData;
import es.boalis.security.auth.exception.RestrictionException;

/**
 * Created by santi on 7/10/17.
 */
public abstract class VerifyConditionAction {

    public abstract void check(SharedData<String,?> data)throws RestrictionException;

}
