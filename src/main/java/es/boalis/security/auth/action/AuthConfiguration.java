package es.boalis.security.auth.action;

import java.util.List;

/**
 * Created by santi on 7/10/17.
 */
public interface AuthConfiguration {

    T getValue(String cat,String key,Class<T> type);
    List<String> getAsList(String cat, String key);
}
