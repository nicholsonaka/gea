package es.boalis.security.auth.action;

import es.boalis.security.auth.bean.*;
import es.boalis.security.auth.exception.AuthenticationException;

import java.util.List;

/**
 * Created by santi on 7/10/17.
 */
public class AuthenticationFlow {

    private AuthConfiguration configuration;

    private Flow flow;

    public AuthConfiguration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(AuthConfiguration configuration) {
        this.configuration = configuration;
    }

    public Flow getFlow() {
        return flow;
    }

    public void setFlow(Flow flow) {
        this.flow = flow;
    }

    public List<Credential> getCredentials(AuthenticationData data, boolean publicFlow, SharedData<String,?>shared)throws AuthenticationException{

        if (shared == null){
            shared = new SharedData<String, Object>();
        }
        String fullNameFlow;
        if(publicFlow){
            fullNameFlow =  "public."+data.getAuthType();
        }else{
            fullNameFlow =  "private."+data.getAuthType();
        }

        List<String> listBeans = this.getConfiguration().getAsList(data.getApplicationId(),fullNameFlow);
        for (String bean: listBeans){
            String [] tupple = bean.split("\\|");
            ActionBean actionBean = new ActionBean(tupple[0],tupple[1]);
            switch (actionBean.getType()) {
                case "MP":
                    //mapping action
                    MappingAction mappingAction = this.getFlow().getMappingFactory().getInstance(actionBean.getName());


                    // TODO implementar el metodo
                    break;
                case "FA":
                    //flow action
                    FlowAction flow = this.getFlow().getFlowFactory().getInstance(actionBean.getName());
                    flow.setAction((String)shared.get("ROUTE.TARGET.FLOW"));
                    flow.setAuthFlow(this);
                    return flow.getCredentials(data,shared);

                case "VA":
                    //verify
                    VerifyConditionAction verify = this.getFlow().getVerifyConditionFactory().getInstance(actionBean.getName());
                    // TODO
                    break;
                case "GC":
                    //generate creds
                    CredentialGenerationAction generate = this.getFlow().getGenerationFactory().getInstance(actionBean.getName());
                    return generate.getCredentias(data,shared);

                case "VC":
                    //validate creds
                    CredencialValidationAction validationCreds = this.getFlow().getValidationFactory().getInstance(actionBean.getName());

                    break;


                default:
                    //
                    throw new AuthenticationException("Method not found");
            }

        }
        throw new AuthenticationException("Generate Credentials action not returned");



        


    }


}
