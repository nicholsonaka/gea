package es.boalis.security.auth.action;

import es.boalis.security.auth.bean.AuthenticationData;
import es.boalis.security.auth.bean.Credential;
import es.boalis.security.auth.bean.SharedData;
import es.boalis.security.auth.exception.AuthenticationException;

import java.util.List;

/**
 * Created by santi on 7/10/17.
 */
public class FlowAction {
    private String action;

    private AuthenticationFlow authFlow;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public AuthenticationFlow getAuthFlow() {
        return authFlow;
    }

    public void setAuthFlow(AuthenticationFlow authFlow) {
        this.authFlow = authFlow;
    }

    public List<Credential> getCredentials(AuthenticationData data, SharedData<String, ?> shared)
            throws AuthenticationException{
        return this.getAuthFlow().getCredentials(data,false,shared);

    }


}
