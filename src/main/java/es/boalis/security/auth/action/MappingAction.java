package es.boalis.security.auth.action;

import es.boalis.security.auth.bean.Attribute;
import es.boalis.security.auth.bean.SharedData;
import es.boalis.security.auth.exception.DataMappingException;

/**
 * Created by santi on 7/10/17.
 */
public abstract class MappingAction {

    public abstract SharedData<String,?> getMappingData(Attribute searchAttribute, String value,SharedData<String,?> shared)
            throws DataMappingException;

}
