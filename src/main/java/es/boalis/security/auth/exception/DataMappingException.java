package es.boalis.security.auth.exception;

/**
 * Created by santi on 7/10/17.
 */
public class DataMappingException extends AuthenticationException {
    public DataMappingException(String field) {
        super(String.format("Field exception %s not found. ",field));
        this.setCode("000 000 002");
    }
}
