package es.boalis.security.auth.exception;

/**
 * Created by santi on 7/10/17.
 */
public class CredentialsGenerationException extends AuthenticationException {
    public CredentialsGenerationException(String reason) {
        super(String.format("Credentials generation failed. Reason : %s . ",reason));
        this.setCode("000 000 005");
    }
}
