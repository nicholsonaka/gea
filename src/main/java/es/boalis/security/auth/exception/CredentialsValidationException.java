package es.boalis.security.auth.exception;

/**
 * Created by santi on 7/10/17.
 */
public class CredentialsValidationException extends AuthenticationException {
    public CredentialsValidationException(String reason) {
        super(String.format("Authentication failed. Reason : %s . ",reason));
        this.setCode("000 000 004");
    }
}
