package es.boalis.security.auth.exception;

/**
 * Created by santi on 7/10/17.
 */
public class AuthenticationDataException extends AuthenticationException {
    public AuthenticationDataException(String field) {
        super(String.format("Field for data %s not found. ",field));
        this.setCode("000 000 001");
    }
}
