package es.boalis.security.auth.exception;

/**
 * Created by santi on 7/10/17.
 */
public class AuthenticationException extends Exception{
    private String code;

    public AuthenticationException() {
    }

    public AuthenticationException(String message) {
        super(message);
    }

    public AuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthenticationException(Throwable cause) {
        super(cause);
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "AuthenticationException{" +
                "code='" + code + '\'' +
                '}';
    }
}
