package es.boalis.security.auth.exception;

/**
 * Created by santi on 7/10/17.
 */
public class InstanceTypeNotFound extends AuthenticationException {
    public InstanceTypeNotFound(String field) {
        super(String.format("Instance %s not found. ",field));
        this.setCode("000 000 0010");
    }
}
