package es.boalis.security.auth.exception;

/**
 * Created by santi on 7/10/17.
 */
public class RestrictionException extends AuthenticationException {
    public RestrictionException(String field) {
        super(String.format("Restriction for checking %s not found. ",field));
        this.setCode("000 000 003");
    }
}
