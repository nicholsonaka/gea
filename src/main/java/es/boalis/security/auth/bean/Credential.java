package es.boalis.security.auth.bean;

/**
 * Created by santi on 7/10/17.
 */
public class Credential {

    private CredentialType type;
    private byte[] content;

    public CredentialType getType() {
        return type;
    }

    public void setType(CredentialType type) {
        this.type = type;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
}
