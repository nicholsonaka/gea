package es.boalis.security.auth.bean;

import es.boalis.security.auth.factory.*;

/**
 * Created by santi on 7/10/17.
 */
public class Flow {
    private String name;

    private CredentialValidationFactory validationFactory;

    private CredentialGenerationFactory generationFactory;

    private FlowFactory flowFactory;

    private MappingFactory mappingFactory;

    private VerifyConditionFactory verifyConditionFactory;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CredentialValidationFactory getValidationFactory() {
        return validationFactory;
    }

    public void setValidationFactory(CredentialValidationFactory validationFactory) {
        this.validationFactory = validationFactory;
    }

    public CredentialGenerationFactory getGenerationFactory() {
        return generationFactory;
    }

    public void setGenerationFactory(CredentialGenerationFactory generationFactory) {
        this.generationFactory = generationFactory;
    }

    public FlowFactory getFlowFactory() {
        return flowFactory;
    }

    public void setFlowFactory(FlowFactory flowFactory) {
        this.flowFactory = flowFactory;
    }

    public MappingFactory getMappingFactory() {
        return mappingFactory;
    }

    public void setMappingFactory(MappingFactory mappingFactory) {
        this.mappingFactory = mappingFactory;
    }

    public VerifyConditionFactory getVerifyConditionFactory() {
        return verifyConditionFactory;
    }

    public void setVerifyConditionFactory(VerifyConditionFactory verifyConditionFactory) {
        this.verifyConditionFactory = verifyConditionFactory;
    }
}
