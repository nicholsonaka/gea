package es.boalis.security.auth.bean;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by santi on 7/10/17.
 */
public class SharedData<String,V> {
    private Map<String,V> map = new HashMap<String, V>();


    public void set(String k, V v){
        this.map.put(k, v);
    }


    public V get(String k){
        return this.map.get(k);
    }

    public boolean containsKey(String k){
        return this.map.containsKey(k);
    }

    public Iterator<String> getKeys(){
        return this.map.keySet().iterator();
    }






}
