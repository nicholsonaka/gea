package es.boalis.security.auth.bean;

/**
 * Created by santi on 7/10/17.
 */
public enum CredentialType {
    PASSWORD,TOKEN,NULL;


    public static CredentialType createPasswordType(){
        return CredentialType.PASSWORD;
    }
    public static CredentialType createTokenType(){
        return CredentialType.TOKEN;
    }

    public CredentialType parse(String t){
        if (t.equals(TOKEN.name())){
            return createTokenType();
        }else if(t.equals(PASSWORD.name())){
            return createPasswordType();
        }else{
            return CredentialType.NULL;
        }
    }

}
