package es.boalis.security.auth.bean;

import java.util.List;

/**
 * Created by santi on 7/10/17.
 */
public class AuthenticationData {
    private List<Credential> credential;
    private String applicationId;
    private String authType;
    private String identifier;
    private String identifierAttribute;


    public List<Credential> getCredential() {
        return credential;
    }

    public void setCredential(List<Credential> credential) {
        this.credential = credential;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifierAttribute() {
        return identifierAttribute;
    }

    public void setIdentifierAttribute(String identifierAttribute) {
        this.identifierAttribute = identifierAttribute;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthenticationData that = (AuthenticationData) o;

        if (credential != null ? !credential.equals(that.credential) : that.credential != null) return false;
        if (applicationId != null ? !applicationId.equals(that.applicationId) : that.applicationId != null)
            return false;
        if (authType != null ? !authType.equals(that.authType) : that.authType != null) return false;
        if (identifier != null ? !identifier.equals(that.identifier) : that.identifier != null) return false;
        return identifierAttribute != null ? identifierAttribute.equals(that.identifierAttribute) : that.identifierAttribute == null;
    }

    @Override
    public int hashCode() {
        int result = credential != null ? credential.hashCode() : 0;
        result = 31 * result + (applicationId != null ? applicationId.hashCode() : 0);
        result = 31 * result + (authType != null ? authType.hashCode() : 0);
        result = 31 * result + (identifier != null ? identifier.hashCode() : 0);
        result = 31 * result + (identifierAttribute != null ? identifierAttribute.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AuthenticationData{" +
                "credential=" + credential +
                ", applicationId='" + applicationId + '\'' +
                ", authType='" + authType + '\'' +
                ", identifier='" + identifier + '\'' +
                ", identifierAttribute='" + identifierAttribute + '\'' +
                '}';
    }
}
