package es.boalis.security.auth.bean;

/**
 * Created by santi on 7/10/17.
 */
public class ActionBean {
    private String type;
    private String name;

    public ActionBean(String type, String name) {
        this.type = type;
        this.name = name;
    }

    @Override
    public String toString() {
        return "ActionBean{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
